package org.example;

import org.example.sorts.SortAscending;

import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        int[] numbers = ReadIntegers.readInts(args);
        SortAscending sortAscending = new SortAscending();
        sortAscending.sort(numbers);
    }
}
