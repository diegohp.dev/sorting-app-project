package org.example;

import java.util.Scanner;

public class ReadIntegers {
    /**
     * @param args Array of Strings
     * @throws IllegalArgumentException throws this exception when the number of
     *                                  arguments is greater than 10
     * @throws NumberFormatException    throws this exception when the arguments contain a
     *                                  non-valid string representation of an integer
     */
    static int[] readInts(String[] args){

        if (args.length > 10)
            throw new IllegalArgumentException("No more than 10 integers");

        int[] numbers = new int[args.length];

        for (int i = 0; i < args.length; i++) {
            try {
                numbers[i] = Integer.parseInt(args[i]);
            } catch (NumberFormatException e) {
                throw new NumberFormatException("Introduce valid integers");
            }
        }

        return numbers;
    }
}
