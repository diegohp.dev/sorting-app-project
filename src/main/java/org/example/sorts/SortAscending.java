package org.example.sorts;

import java.util.Arrays;

public class SortAscending implements Sortable {
    /**
     * @param numbers Array of integers;
     * @apiNote Sorts an integer array in-place
     */
    @Override public void sort(int[] numbers){
        Arrays.sort(numbers);
        System.out.println("The numbers in ascending order are: ");
        System.out.print("[ ");
        for (int num : numbers) {
            System.out.print(num + " ");
        }
        System.out.println("]");
    }
}
