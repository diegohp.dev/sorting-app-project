package org.example.sorts;

public interface Sortable {

    public void sort(int[] numbers);
}
