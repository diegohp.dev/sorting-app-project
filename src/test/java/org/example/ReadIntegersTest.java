package org.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;


/**
 * Unit tests for ReadIntegers Class.
 */
@RunWith(Parameterized.class)
public class ReadIntegersTest {
    private final String[] args;
    private final int[] expected;

    public ReadIntegersTest(String[] args, int[] expected){
        this.args = args;
        this.expected = expected;
    }

    /**
     * Test to ensure that the number of introduced integers are less or equal to 10
     * */
    @Test(expected = IllegalArgumentException.class)
    public void testMoreThan10Args(){
        ReadIntegers.readInts(new String[]{"5","3","8","4","22","11","-4","0","-8","9",
            "6"});
    }

    /**
     * Test to ensure that the arguments are valid integers
     * */

    @Test(expected = NumberFormatException.class)
    public void testValidIntegers(){
        ReadIntegers.readInts(new String[]{"8","3","7","4","a","h"});
    }

    /**
     * Parameterized tests for different valid arguments
     * */
    @Parameterized.Parameters public static Collection<Object[]> data(){
        return Arrays.asList(
            new Object[][]{
                {new String[]{},new int[]{}},
                {new String[]{"8"},new int[]{8}},
                {new String[]{"3","0","33","8","0","1","10","7","14","4"},
                    new int[]{3,0,33,8,0,1,10,7,14,4}},
                {new String[]{"1","0","5","7","9","8","6","3","2"},
                    new int[]{1,0,5,7,9,8,6,3,2}}

            }
        );
    }

    @Test
    public void testIntArrayGeneration(){
        int[] result = ReadIntegers.readInts(this.args);
        assertArrayEquals(this.expected,result);
    }
}
