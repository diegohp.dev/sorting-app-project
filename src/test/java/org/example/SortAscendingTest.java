package org.example;

import org.example.sorts.SortAscending;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;


@RunWith(Parameterized.class)
public class SortAscendingTest {
    private final int[] numbers;
    private final int[] expected;

    private final SortAscending sortAscending = new SortAscending();

    public SortAscendingTest(int[] numbers,int[] expected){
        this.numbers = numbers;
        this.expected = expected;
    }

    /**
     * Parameterized tests for different valid arguments
     * */
    @Parameterized.Parameters public static Collection<Object[]> data(){
        return Arrays.asList(new Object[][]{
            {new int[]{},new int[]{}},
            {new int[]{2},new int[]{2}},
            {new int[]{4,8,6,2,7,19,30,57,28},new int[]{2,4,6,7,8,19,28,30,57}},
            {new int[]{1,2,3,4,5,6,7,8,9,10},new int[]{1,2,3,4,5,6,7,8,9,10}},
            {new int[]{0,-4,-9,7,4,2,5,3,-100},new int[]{-100,-9,-4,0,2,3,4,5,7}}
        });

    }

    @Test
    public void testSortAscending(){
        sortAscending.sort(this.numbers);
        assertArrayEquals(this.expected,this.numbers);
    }
}
